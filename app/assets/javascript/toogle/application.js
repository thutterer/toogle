document.addEventListener("alpine:init", () => {
  Alpine.data("features", (indexUrl) => ({
    features: [],
    indexUrl,

    init() {
      fetch(this.indexUrl, {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      })
        .then((response) => response.json())
        .then((data) => {
          this.features = data;
        })
        .catch((error) => console.error("Error fetching data:", error));
    },

    toggleFeature(feature) {
      const newState = feature.state === "enabled" ? "disabled" : "enabled";
      fetch(`${this.indexUrl}/${feature.name}`, {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          state: newState,
        }),
      }).then(() => {
        this.features = this.features.map((f) => {
          return f === feature ? { ...f, state: newState } : f;
        });
      });
    },

    deleteFeature(featureName) {
      const csrfToken = document.head.querySelector(
        "meta[name=csrf-token]"
      )?.content;

      fetch(`${this.indexUrl}/${featureName}`, {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "X-CSRF-Token": csrfToken,
        },
      }).then(() => {
        this.features = this.features.filter((f) => f.name !== featureName);
      });
    },
  }));

  Alpine.data("toggle", (featureName, isChecked, indexUrl) => ({
    name: featureName,
    checked: isChecked,
    indexUrl,

    input: {
      ["@change"]() {
        const csrfToken = document.head.querySelector(
          "meta[name=csrf-token]"
        )?.content;

        fetch(`${this.indexUrl}/${this.name}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            "X-CSRF-Token": csrfToken,
          },
          body: JSON.stringify({
            state: this.checked ? "disabled" : "enabled",
          }),
        }).then(() => {
          window.location = this.indexUrl;
        });
      },
    },
  }));

  Alpine.data("darkModeSwitcher", () => ({
    isDark: undefined,

    init() {
      const cookieValue = this.getCookieValue();
      if (cookieValue) {
        this.isDark = cookieValue === "true";
      } else {
        this.isDark = window.matchMedia("(prefers-color-scheme: dark)").matches;
      }
    },

    getCookieValue() {
      return (
        document.cookie
          .match("(^|;)\\s*" + "toogle-dark" + "\\s*=\\s*([^;]+)")
          ?.pop() || ""
      );
    },

    button: {
      ["@click"]() {
        this.isDark = !this.isDark;
        document.cookie = `toogle-dark=${this.isDark}; SameSite=Strict`;
        if (this.isDark) {
          document.documentElement.classList.remove("light-mode");
        } else {
          document.documentElement.classList.add("light-mode");
        }
      },
      ["x-bind:title"]() {
        return `Switch to ${this.isDark ? "Light Mode" : "Dark Mode"}`;
      },
    },
  }));
});
