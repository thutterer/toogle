# A simple wrapper around GitLab's Feature:Definition class for easier use and testing.

module Toogle
  class Definition
    attr_accessor :name, :default_enabled

    def self.all
      ::Feature::Definition.definitions.map do |definition|
        new(
          name: definition[0].to_s,
          default_enabled: definition[1].default_enabled,
          milestone: definition[1].milestone,
          introduced_by_url: definition[1].introduced_by_url,
          rollout_issue_url: definition[1].rollout_issue_url,
        )
      end
    end

    def self.unchanged
      changed_features = Toogle::Feature.all.map(&:name)
      all.reject { |definition| changed_features.include?(definition.name) }
    end

    def self.find(name)
      all.find { |definition| definition.name == name }
    end

    def initialize(name:, default_enabled:, milestone: nil, introduced_by_url: nil, rollout_issue_url: nil)
      @name = name
      @default_enabled = default_enabled
      @milestone = milestone
      @introduced_by_url = introduced_by_url
      @rollout_issue_url = rollout_issue_url
    end
  end
end
