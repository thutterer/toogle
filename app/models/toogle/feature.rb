# A simple wrapper around GitLab's Feature class for easier use and testing.

module Toogle
  class Feature
    attr_accessor :name, :state, :definition

    def self.all
      definitions = Definition.all

      Flipper::Adapters::ActiveRecord.new(
        feature_class: ::Feature::FlipperFeature,
        gate_class: ::Feature::FlipperGate
      ).get_all.map do |feature_name, feature_values|
        feature_definition = Definition.find(feature_name)
        feature_state = if feature_definition
          feature_values[:boolean] ? :enabled: :disabled
        else
          # This usually happens when switching back from an unmerged feature
          # branch that introduces a new flag, or when a flag got deleted.
          :unknown
        end
        new(name: feature_name, state: feature_state, definition: feature_definition)
      end
    end

    def initialize(name:, state:, definition:)
      @name = name
      @state = state
      @definition = definition
    end
  end
end
