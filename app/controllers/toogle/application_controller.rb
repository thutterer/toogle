module Toogle
  class ApplicationController < ActionController::Base
    # In order for Alpine to be able to execute plain strings from HTML attributes as JavaScript expressions,
    # for example x-on:click="console.log()", it needs to rely on utilities that violate the "unsafe-eval"
    #  content security policy.
    content_security_policy false, if: -> { Rails.env.development? }

    # Prevent all browser caching. Otherwise a cached version of a page might show the wrong toggle state.
    # Also, it really doesn't matter on localhost :)
    before_action :set_cache_headers

    private

    def set_cache_headers
      response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
      response.headers["Pragma"] = "no-cache"
      response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
    end
  end
end
