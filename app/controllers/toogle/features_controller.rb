module Toogle
  class FeaturesController < ApplicationController
    def index
      @features = Toogle::Feature.all

      respond_to do |format|
        format.html
        format.json { render json: @features }
      end
    end

    def show
      @definition = Toogle::Definition.find(params[:id])
      if @definition.nil?
        flash[:notice] = "No feature defintion with name \"#{params[:id]}\" found."
      end
      index
      render "index"
    end

    def update
      id = params[:id].to_sym

      if params[:state] == "enabled"
        ::Feature.enable(id)
      elsif params[:state] == "disabled"
        ::Feature.disable(id)
      end

      respond_to do |format|
        format.html do
          redirect_to features_url
        end

        format.json do
          head :ok
        end
      end
    end

    def destroy
      ::Feature.remove(params[:id])

      respond_to do |format|
        format.html do
          redirect_to features_url, status: :see_other
        end

        format.json do
          head :ok
        end
      end
    end
  end
end
