module Toogle
  class DefinitionsController < ApplicationController
    # This controller is only used to fetch the long list of feature definitions async.
    layout false

    def index
      @definitions = Toogle::Definition.unchanged
    end
  end
end
