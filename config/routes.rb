Toogle::Engine.routes.draw do
  resources :definitions, only: %i[index]

  # Keep this line last as it matches any URL
  resources :features, only: %i[index show update destroy], path: "/"
end
