RSpec::Matchers.define :have_toggle do |text, status|
  match do |page|
    expect(page).to have_selector(".feature-toggle", text: text)

    checkbox = page.find_field(text, visible: :all, disabled: :all)
    if status == :enabled
      expect(checkbox).to be_checked
    elsif status == :disabled
      expect(checkbox).not_to be_checked
    elsif status == :unknown
      expect(checkbox).to be_disabled
    else
      raise "Unknown status: #{status}"
    end
  end
end
