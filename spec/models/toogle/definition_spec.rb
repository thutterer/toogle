require "rails_helper"

RSpec.describe Toogle::Definition do
  before do
    allow(Toogle::Definition).to receive(:all).and_return(all_definitions)
  end

  let(:all_definitions) {
    [
      described_class.new(name: "foo", default_enabled: true),
      described_class.new(name: "bar", default_enabled: false)
    ]
  }

  describe "unchanged" do
    subject { described_class.unchanged }

    before do
      allow(Toogle::Feature).to receive(:all).and_return(changed_features)
    end

    context "with changed features" do
      let(:changed_features) { [Toogle::Feature.new(name: "foo", state: :enabled, definition: all_definitions.first)] }

      it { is_expected.to eq([all_definitions.last]) }
    end

    context "with no changed features" do
      let(:changed_features) { [] }

      it { is_expected.to eq(all_definitions) }
    end
  end

  describe "find" do
    subject { described_class.find(name) }

    let(:all_definitions) {
      [
        described_class.new(name: "foo", default_enabled: true),
        described_class.new(name: "bar", default_enabled: false)
      ]
    }

    context "with matching name" do
      let(:name) { "foo" }
      it { is_expected.to eq(all_definitions.first) }
    end

    context "when name does not match" do
      let(:name) { "baz" }
      it { is_expected.to be_nil }
    end
  end
end
