require "rails_helper"

describe Toogle::FeaturesController do
  before do
    stub_const("Feature", Class.new {
      def self.enable(feature_name)
        true
      end

      def self.disable(feature_name)
        true
      end
    })
  end

  describe "PUT #update" do
    let(:feature_name) { :some_feature }

    context "when enabling a feature" do
      it "calls Feature.enable with the feature id" do
        expect(Feature).to receive(:enable).with(feature_name)
        put "/toogle/#{feature_name}", params: {state: "enabled"}
      end
    end

    context "when disabling a feature" do
      it "calls Feature.disable with the feature id" do
        expect(Feature).to receive(:disable).with(feature_name)
        put "/toogle/#{feature_name}", params: {state: "disabled"}
      end
    end

    it "redirects to the root url" do
      put "/toogle/#{feature_name}", params: {state: "enabled"}
      expect(response).to redirect_to(root_url)
    end
  end
end
