require "rails_helper"

describe "List of active feature flags" do
  before do
    allow(Toogle::Feature).to receive(:all).and_return(features)
    visit "/toogle"
  end

  context "with no active feature flags" do
    let(:features) { [] }

    it "shows a message" do
      expect(page).to have_content "No active feature flags"
    end
  end

  context "with active feature flags" do
    let(:mock_definition) { Toogle::Definition.new(name: "doesnt_matter", default_enabled: false) }

    let(:features) {
      [
        Toogle::Feature.new(name: "foo", state: :enabled, definition: mock_definition),
        Toogle::Feature.new(name: "bar", state: :disabled, definition: mock_definition),
        Toogle::Feature.new(name: "baz", state: :unknown, definition: nil)
      ]
    }

    it "lists them all with correct toggle state" do
      expect(page).to have_toggle("foo", :enabled)
      expect(page).to have_toggle("bar", :disabled)
      expect(page).to have_toggle("baz", :unknown)
    end
  end
end
