require "rails_helper"

describe "Feature share link" do
  let(:foo_definition) { Toogle::Definition.new(name: "foo", default_enabled: true) }

  before do
    allow(Toogle::Feature).to receive(:all).and_return([
      Toogle::Feature.new(name: "foo", state: :disabled, definition: foo_definition)
    ])
    allow(Toogle::Definition).to receive(:all).and_return([
      foo_definition,
      Toogle::Definition.new(name: "bar", default_enabled: false)
    ])
  end

  context "with an active feature flag name as id" do
    it "shows a dialog to toggle this feature" do
      visit "/toogle/foo"
      expect(page).to have_css "dialog", text: "foo\nClick the toggle"
    end
  end

  context "with an unchanged but existing feature definition as id" do
    it "shows a dialog to toggle this feature" do
      visit "/toogle/bar"
      expect(page).to have_css "dialog", text: "bar\nClick the toggle"
    end
  end

  context "with an id that does not match any feature definition" do
    it "shows no dialog" do
      visit "/toogle/baz"
      expect(page).not_to have_css "dialog"
      expect(page).to have_text "No feature defintion with name \"baz\""
    end
  end

  context "with no id" do
    it "shows no dialog" do
      visit "/toogle"
      expect(page).not_to have_css "dialog"
    end
  end
end
