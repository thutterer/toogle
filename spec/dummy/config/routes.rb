Rails.application.routes.draw do
  root to: redirect("/")
  mount Toogle::Engine => "/toogle"
end
