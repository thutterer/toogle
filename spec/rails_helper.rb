require "spec_helper"
require_relative "support/capybara_custom_matchers"

ENV["RAILS_ENV"] ||= "test"

require_relative "dummy/config/environment"

# Prevent database truncation if the environment is production
abort("The Rails environment is running in production mode!") if Rails.env.production?

require "rspec/rails"
# Add additional requires below this line. Rails is not loaded until this point!

Capybara.server = :webrick

RSpec.configure do |config|
  config.use_active_record = false
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!

  config.before(:each, type: :system) do
    driven_by :selenium_headless
  end

  config.before(:each) do
    Rails::Application::Configuration.new(Rails.root).paths["app/views"]
  end
end
