require_relative "lib/toogle/version"

Gem::Specification.new do |spec|
  spec.name = "gdk-toogle"
  spec.version = Toogle::VERSION
  spec.authors = ["Thomas Hutterer"]
  spec.email = ["thutterer@gitlab.com"]
  spec.homepage = "https://gitlab.com/thutterer/toogle"
  spec.summary = "Toogle toggles feature flags"
  spec.description = "Toogle is a Rails engine to toggle feature flags in GitLab development environments."
  spec.license = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/thutterer/toogle"
  spec.metadata["changelog_uri"] = "https://gitlab.com/thutterer/toogle"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "rails", ">= 7.0.4.2"
  spec.add_dependency "haml"
  spec.add_development_dependency "rspec-rails"
  spec.add_development_dependency "capybara"
  spec.add_development_dependency "selenium-webdriver"
end
