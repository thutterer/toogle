# Toogle

A Rails engine web-UI to quickly toggle feature flags in GDK.

## Usage

This gem was written specifically for the GitLab codebase. It won't work in any other Rails app.

### Usage within the GitLab Development Kit

The UI is availabe at http://gdk.test:3000/rails/features.

## Contributing

Everyone can contribute.

### How to run tests

This is the default rake task. Just run `rake` with no arguments.

The system specs in `spec/system` are configured to use Firefox as headless browser.

**Tip:** If you don't have Firefox installed on your machine, or if you are on Ubuntu and have Firefox installed only via Snap, you can switch to Chrome by changing the `driven_by` to `:selenium_chrome_headless` in `spec/rails_helper.rb`.

## Tech stack

One design goal of this little Rails engine was to keep it free of any dependencies, that GitLab uses today.
Because, tomorrow we might decide to ditch one or the other tech we use today, and Toogle shouldn't break just because GitLab removes `sassc`, as an example (that actually happened during development).

That's why Toogle only depends on the bare basics: `rails`, `sprockets` and `haml`.

### Javascript

Toogle uses [Alpine.js](https://alpinejs.dev/) from a `<script>` tag so it does not use or depend on our Vue.js usage or any bundler. Alpine is very similar to Vue, so it's really easy to pick up. Give it a try!

### CSS

Toogle uses native unprocessed CSS to not depend on any specific preprocessor. Modern CSS is awesome, and all modern browsers support things like nested selectors today.

For theming and dark mode, it uses CSS variables and HSL colors, which makes it super easy to switch from light to dark by just changing the variable that makes up the _Lightness_. Check it out!

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
